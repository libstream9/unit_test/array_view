#include <stream9/array_view.hpp>

#include <ranges>
#include <type_traits>
#include <vector>

#include <boost/test/unit_test.hpp>

#include <stream9/optional.hpp>

namespace testing {

namespace st9 = stream9;

using st9::array_view;

template<typename T>
array_view<T>
subrange(array_view<T> v,
         typename array_view<T>::size_type offset) noexcept
{
    auto end = v.end();

    auto f = v.begin() + offset;
    if (f > end) f = end;

    return { f, end };
}

template<typename T>
array_view<T>
subrange(array_view<T> v,
         typename array_view<T>::size_type offset,
         typename array_view<T>::size_type length) noexcept
{
    auto end = v.end();

    auto f = v.begin() + offset;
    if (f > end) f = end;

    auto l = f + length;
    if (l > end) l = end;

    return { f, l };
}

BOOST_AUTO_TEST_SUITE(array_view_)

    BOOST_AUTO_TEST_SUITE(constructor_)

        BOOST_AUTO_TEST_CASE(default_)
        {
            static_assert(!std::is_default_constructible_v<st9::array_view<int>>);
        }

        BOOST_AUTO_TEST_CASE(array_)
        {
            int arr[] = { 1, 2, 3 };

            st9::array_view v(arr);

            BOOST_TEST(v.size() == 3);
        }

        BOOST_AUTO_TEST_CASE(vector_)
        {
            std::vector<int> arr { 1, 2, 3 };

            st9::array_view v(arr);

            BOOST_TEST(v.size() == 3);
        }

        BOOST_AUTO_TEST_CASE(empty_)
        {
            std::vector<int> arr;

            st9::array_view v(arr);

            BOOST_TEST(v.size() == 0);
        }

        BOOST_AUTO_TEST_CASE(iterator_and_size_)
        {
            std::vector<int> arr { 1, 2, 3 };

            st9::array_view v(arr.begin(), arr.size());

            BOOST_TEST(v.size() == 3);
        }

        BOOST_AUTO_TEST_CASE(iterator_range_)
        {
            std::vector<int> arr { 1, 2, 3 };

            st9::array_view v(arr.begin(), arr.end());

            BOOST_TEST(v.size() == 3);
        }

    BOOST_AUTO_TEST_SUITE_END() // constructor_

    BOOST_AUTO_TEST_CASE(front_)
    {
        int arr[] { 1, 2, 3 };

        st9::array_view v(arr);

        BOOST_TEST(v.front() == 1);
    }

    BOOST_AUTO_TEST_CASE(back_)
    {
        int arr[] { 1, 2, 3 };

        st9::array_view v(arr);

        BOOST_TEST(v.back() == 3);
    }

    BOOST_AUTO_TEST_CASE(subscript_)
    {
        int arr[] { 1, 2, 3 };

        st9::array_view v(arr);

        BOOST_TEST(v[0] == 1);
        BOOST_TEST(v[1] == 2);
        BOOST_TEST(v[2] == 3);
    }

    BOOST_AUTO_TEST_CASE(data_)
    {
        int arr[] { 1, 2, 3 };

        st9::array_view v(arr);

        BOOST_TEST(v.data() == arr);
    }

    BOOST_AUTO_TEST_CASE(begin_)
    {
        int arr[] { 1, 2, 3 };

        st9::array_view v(arr);

        BOOST_CHECK(v.begin() == arr);
    }

    BOOST_AUTO_TEST_CASE(end_)
    {
        int arr[] { 1, 2, 3 };

        st9::array_view v(arr);

        BOOST_CHECK(v.end() == arr + 3);
    }

    BOOST_AUTO_TEST_CASE(empty_1_)
    {
        std::vector<int> arr;

        st9::array_view v(arr);

        BOOST_CHECK(v.empty());
    }

    BOOST_AUTO_TEST_CASE(empty_2_)
    {
        int arr[] { 1, 2, 3 };

        st9::array_view v(arr);

        BOOST_CHECK(!v.empty());
    }

#if 0
    BOOST_AUTO_TEST_CASE(resize_)
    {
        int arr[] { 1, 2, 3 };

        st9::array_view v(&arr[0], 2);

        v.resize(1);
        BOOST_CHECK(v.size() == 1);

        v.resize(3);
        BOOST_CHECK(v.size() == 3);
    }

    BOOST_AUTO_TEST_CASE(advance_begin_)
    {
        int arr[] { 1, 2, 3 };

        st9::array_view v(arr);

        v.advance_begin(1);
        BOOST_TEST(v[0] == 2);

        v.advance_begin(-1);
        BOOST_TEST(v[0] == 1);
    }

    BOOST_AUTO_TEST_CASE(advance_end_)
    {
        int arr[] { 1, 2, 3 };

        st9::array_view v(&arr[0], 2);

        v.advance_end(1);
        BOOST_TEST(v.size() == 3);

        v.advance_end(-1);
        BOOST_TEST(v.size() == 2);
    }
#endif

    BOOST_AUTO_TEST_CASE(optional_)
    {
        using stream9::array_view;
        using stream9::opt;

        opt<array_view<int>> o_v;

        static_assert(sizeof(o_v) == sizeof(array_view<int>));
    }

    BOOST_AUTO_TEST_CASE(subrange_1_)
    {
        int arr[] = { 1, 2, 3 };

        st9::array_view v1 { arr };
        BOOST_TEST(v1.size() == 3);

        array_view v2 { v1, 2 };
        BOOST_CHECK(v2.begin() == v1.begin() + 2);
        BOOST_CHECK(v2.end() == v1.end());

        array_view v3 { v1, 5 };
        BOOST_CHECK(v3.begin() == v1.end());
        BOOST_CHECK(v3.end() == v1.end());
    }

    BOOST_AUTO_TEST_CASE(subrange_2_)
    {
        int arr[] = { 1, 2, 3 };

        array_view v2 { arr, 0, 1 };
        BOOST_CHECK(v2.begin() == &arr[0]);
        BOOST_CHECK(v2.end() == &arr[1]);

        array_view v3 { arr, 5, 1 };
        BOOST_CHECK(v3.begin() == &arr[3]);
        BOOST_CHECK(v3.end() == &arr[3]);

        array_view v4 { arr, 0, 5 };
        BOOST_CHECK(v4.begin() == &arr[0]);
        BOOST_CHECK(v4.end() == &arr[3]);
    }

BOOST_AUTO_TEST_SUITE_END() // array_view_

} // namespace testing
